package crypto.helper;

import java.io.InputStream;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Lifaen
 */


public class ResourceHelper {
    
    static public Icon getIcon(String name) {
        String path = "crypto/resource/icon/"+ name;
        
        URL url = Thread.currentThread().getContextClassLoader().getResource(path);
        if (url != null) {
            return new ImageIcon(url);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }     
}
