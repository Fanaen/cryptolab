package crypto.model;

/**
 *
 * @author Lifaen
 */


public class Char implements Cloneable {

    // -- Attribute --
    private char character;
    
    // -- Constructor --
    public Char(char d) {
        character = d;
    }

    public Char(Char c) {
        this(c.character);
    }
    
    // -- Getters & Setters --
    public char getCharacter() {
        return character;
    }

    public void setCharacter(char character) {
        this.character = character;
    }
    
    public boolean isLetter() {
        return isUpperCase() || isLowerCase();        
    }
    
    public boolean isUpperCase() {
        int number = (int) (character);
        return number >= 65 && number <= 90;
    }
    
    public boolean isLowerCase() {
        int number = (int) (character);
        return number >= 97 && number <= 122;
    }
    
    public int getNumberLetter() {
        return getNumberLetter(0);
    }
    
    public int getNumberLetter(int offset) {
        int number = (int) (character);
        
        if(isUpperCase()) {
            return number - 65 + offset;
        }
        else if(isLowerCase()) {
            return number - 97 + offset;
        }
        else
            return -1;
    }
    
    public void setNumberLetter(int number) {
        setNumberLetter(number, isUpperCase(), 0);
    }
    
    public void setNumberLetter(int number, int offset) {
        setNumberLetter(number, isUpperCase(), offset);
    }

    private void setNumberLetter(int number, boolean upperCase, int offset) {
        
        // Apply offset --
        number -= offset;
        
        // Set positive --
        while(number < 0) {
            number += 26;
        }
        
        // Reduce if over 25 --
        number = number % 26;
        
        // According to the case, change the number --
        if(upperCase) {
            number += 65;
            character = (char) number;
        }
        else {
            number += 97 ;
            character = (char) number;   
        }
            
    }
}
