package crypto.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lifaen
 */


public class Message {
    
    // -- Attributes --
    List<Char> originList;
    List<Char> newList;
    String message;
    
    // -- Constructors --
    public Message(String message) {
        
        this.message = message;
        extractCharacters(message);
    } 

    // -- Methods --
    private void extractCharacters(String message) {
        
        // Initialise the list --
        originList = new ArrayList<>(message.length());
        newList = new ArrayList<>();
        
        // Populate the list --
        char[] charArray = message.toCharArray();
        for (char d : charArray) {
            originList.add(new Char(d));
        }
    }
    
    public void constructString() {
        StringBuilder builder = new StringBuilder(newList.size());
        
        for(Char c : newList) {
            builder.append(c.getCharacter());
        }
        
        message = builder.toString();
    }
    
    // -- Getters & Setters --

    public List<Char> getOriginList() {
        return originList;
    }

    public void setOriginList(List<Char> charList) {
        this.originList = charList;
    }

    public List<Char> getNewList() {
        return newList;
    }

    public void setNewList(List<Char> newList) {
        this.newList = newList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    
}
