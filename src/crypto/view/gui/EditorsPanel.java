package crypto.view.gui;

import crypto.controller.MainController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

/**
 *
 * @author Lifaen
 */


public class EditorsPanel extends JPanel {
    
    // -- Attributes --
    final MainController controller;
    EditorPanel upperEditor;
    EditorPanel lowerEditor;
    JSplitPane splitPane;
    
    // -- Constructors --
    public EditorsPanel(MainController controller) {
        initComponents();
        this.controller = controller;
    }
    
    // -- Methods --
    private void initComponents() {
        
        // Layout --
        BorderLayout layout = new BorderLayout();
        this.setLayout(layout);
        
        // Editor panes --
        upperEditor = new EditorPanel();
        lowerEditor = new EditorPanel();
        
        // Split pane --
        splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
            upperEditor,
            lowerEditor);
        splitPane.setDividerLocation(0.5);
        
        add(splitPane, BorderLayout.CENTER);
    }

    String getSource() {
        return upperEditor.getMessage();
    }

    void setDestination(String message) {
        lowerEditor.setMessage(message);
    }

    String getDestination() {
        return lowerEditor.getMessage();
    }

    void setSource(String message) {
        upperEditor.setMessage(message);
    }
    
}
