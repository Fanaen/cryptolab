package crypto.view.gui;

import com.alee.global.StyleConstants;
import com.alee.laf.button.WebButton;
import com.alee.laf.toolbar.ToolbarStyle;
import com.alee.laf.toolbar.WebToolBar;
import crypto.helper.ResourceHelper;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 *
 * @author Lifaen
 */


public class EditorPanel extends JPanel {
    
    // -- Attributes --
    JTextArea area;
    
    // -- Constructors --
    public EditorPanel() {
        // Set dimensions --
        Dimension minimumSize = new Dimension(200, 200);
        setMinimumSize(minimumSize);
        
        initComponents();
    }

    // -- Methods --
    private void initComponents() {
        
        // Layout --
        BorderLayout layout = new BorderLayout();
        this.setLayout(layout);
        
        // Toolbar --
        WebToolBar toolbar = new WebToolBar(JToolBar.HORIZONTAL);
        toolbar.setFloatable(false);
        toolbar.setToolbarStyle(ToolbarStyle.attached);
        
        WebButton eraseButton = WebButton.createIconWebButton ((ImageIcon) ResourceHelper.getIcon("delete-2x.png"), StyleConstants.smallRound, true );
        eraseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                area.setText("");
            }
        });
        toolbar.add(eraseButton);
        
        // Text Area --
        area = new JTextArea();
        area.setBounds(new Rectangle(100, 100));
        area.setBackground(new Color(0, 0, 0));
        area.setForeground(new Color(0, 170, 242));
        area.setFont(new Font("Consolas", Font.PLAIN, 16));
        area.setLineWrap(true);
        area.setMargin(new Insets(10, 10, 10, 10));
        
        JScrollPane areaPane = new JScrollPane(area);
        areaPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        
        // Add items --
        add(toolbar, BorderLayout.NORTH);
        add(areaPane, BorderLayout.CENTER);
    }

    String getMessage() {
        return area.getText();
    }

    void setMessage(String message) {
        area.setText(message);
    }
    
}
