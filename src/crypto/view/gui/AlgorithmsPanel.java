package crypto.view.gui;

import crypto.controller.MainController;
import crypto.view.gui.algorithm.Base;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

/**
 *
 * @author Lifaen
 */

public class AlgorithmsPanel extends JPanel {
    
    // -- Attributes --
    private final MainController controller;
    private Base algorithmPanel;
    private MainView parent;
    private JComboBox algorithmList;
    
    // -- Constructors --
    public AlgorithmsPanel(MainController controller, MainView parent) {
        this.controller = controller;
        this.parent = parent;
        initComponents();
    }
    
    // -- Methods --
    private void initComponents(){
        
        // Layout --
        BorderLayout layout = new BorderLayout();
        this.setLayout(layout);
        JPanel buttonPanel = new JPanel(new GridLayout(4, 1));
        
        // Set the algorithm list --
        List<String> stringList = controller.getAlgorithmList();
        algorithmList = new JComboBox();
        buttonPanel.add(algorithmList);
        
        for (String item : stringList) {
            algorithmList.addItem(item);
        }
        
        algorithmList.setSelectedIndex(stringList.indexOf(controller.getAlgorithmName()));
        
        algorithmList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Listener l : listenerList) {
                    l.onAlgorithmChange((String) algorithmList.getSelectedItem());
                }
            }
        });
        
        // Buttons panel --
        JButton encryptButton = new JButton("Encrypt");
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Listener l : listenerList) {
                    l.onEncrypt();
                }
            }
        });
        
        JButton decryptButton = new JButton("Decrypt");
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Listener l : listenerList) {
                    l.onDecrypt();
                }
            }
        });
        
        JButton invertButton = new JButton("Invert");
        invertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (Listener l : listenerList) {
                    l.onInvert();
                }
            }
        });
        
        buttonPanel.add(encryptButton);
        buttonPanel.add(decryptButton);
        buttonPanel.add(invertButton);
        add(buttonPanel, BorderLayout.NORTH);
        
        // Dimension --
        this.setMinimumSize(new Dimension(200, 100));
        this.setPreferredSize(new Dimension(200, 100));
        
        
    }

    public Object getData() {
        return algorithmPanel.getData();
    }

    void changeConfiguration(String configurationName) throws Exception {
        
        if(algorithmPanel != null) {
            remove(algorithmPanel);
        }
        
        // Get the panel --
        String className = "crypto.view.gui.algorithm." + configurationName;
        Class<?> panelClass = Class.forName(className);
        algorithmPanel = (Base) panelClass.newInstance();
        add(algorithmPanel, BorderLayout.CENTER);
    }
    
    // -- Listener --
    private List<Listener> listenerList = new LinkedList<>();
    
    public void addListener(Listener listener) {
        listenerList.add(listener);
    }
    
    public interface Listener {
        void onEncrypt();
        void onDecrypt();
        void onInvert();
        void onAlgorithmChange(String name);
    }
}
