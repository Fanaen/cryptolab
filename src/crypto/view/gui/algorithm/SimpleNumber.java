package crypto.view.gui.algorithm;

import com.alee.laf.spinner.WebSpinner;
import java.awt.Dimension;



public class SimpleNumber extends Base {
    // -- Attributes --
    WebSpinner numberField;
    
    // -- Methods --
    @Override
    protected void initComponents() {
        
        // Text Area --
        numberField = new WebSpinner();
        numberField.setPreferredSize(new Dimension(90, 40));
        numberField.setValue(0);
        
        // Add items --
        add(numberField);
    }
    
    @Override
    public Object getData() {
        return numberField.getValue();
    }
}
