package crypto.view.gui.algorithm;

import com.alee.laf.text.WebTextField;



public class SimpleKey extends Base {
    
    // -- Attributes --
    WebTextField keyField;

    // -- Methods --
    @Override
    protected void initComponents() {
        
        // Text Area --
        keyField = new WebTextField("Key", 15);
        
        // Add items --
        add(keyField);
    }

    @Override
    public Object getData() {
        return keyField.getText();
    }
}
