package crypto.view.gui.algorithm;

import javax.swing.JPanel;

/**
 *
 * @author Lifaen
 */

public abstract class Base extends JPanel {
    
    
    // -- Constructors --
    public Base() {        
        initComponents();
    }
    
    // -- Methods --
    abstract public Object getData(); 
    abstract protected void initComponents(); 
}
