package crypto.algorithm;

import crypto.model.Char;
import java.util.LinkedList;
import java.util.List;
import org.ejml.simple.SimpleMatrix;

/**
 *
 * @author Lifaen
 */


public class Hill extends BaseParseCharacter {

    SimpleMatrix keyMatrix;
    int matrixSize;
    int offset = 0;
    
    List<Char> originList = new LinkedList<>();
    List<Char> newList = new LinkedList<>();
    
    @Override
    public void encrypt() throws Exception {
        retrieveMatrix();
        super.encrypt();
    }

    @Override
    public void decrypt() throws Exception {
        retrieveMatrix();
        reverseMatrix();
        super.decrypt();
    }

    @Override
    public String getDataPanel() {
        return "SimpleKey";
    }


    protected void retrieveMatrix() throws Exception {
        // Get data --
        String matrixString = (String) data;
        String[] matrixValues = matrixString.split(";");
        
        // Check --
        matrixSize = (int) Math.sqrt(matrixValues.length);
        if(matrixSize*matrixSize != matrixValues.length) throw new Exception("Incorrect matrix");
        
        // Create matrix --
        keyMatrix = new SimpleMatrix(matrixSize, matrixSize);
        
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                keyMatrix.set(i, j, Double.parseDouble(matrixValues[i * matrixSize + j]));
            }
        }
    }
    
    protected void reverseMatrix() {
        Double determinant = keyMatrix.determinant();
        keyMatrix.set(keyMatrix.invert());
        keyMatrix.set(keyMatrix.scale(determinant));
                
        int[] dico = {1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25};
        int coefficient = 0;
        for (int cle : dico) {
            int inverse = (int) (determinant * cle % 26); 
            if (inverse == 1) {
                coefficient = cle;
                break;
            }
        }
        
        keyMatrix.set(keyMatrix.scale(coefficient));
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                double value = keyMatrix.get(i, j);
                while (value < 0) {
                    value += 26;
                }
                
                keyMatrix.set(i, j, value % 26);
            }
        }
    }
    
    @Override
    void encryptCharacter(Char c) {
        processCharacter(c);
    }

    @Override
    void decryptCharacter(Char c) {
        processCharacter(c);
    }
    
    protected void processCharacter(Char c) {
        
        // Duplicate the character and add it into the new list --
        Char newChar = new Char(c);
        message.getNewList().add(newChar);
        
        // Ignore non-letter characters --
        if(c.isLetter()) {
            
            // Wait for n items --
            originList.add(c);
            newList.add(newChar);

            if(originList.size() == matrixSize) {

                applyKeyMatrix();

                // Reset the n-items sequence --
                originList.clear();
                newList.clear();
            }
        }
    }
    
    protected void applyKeyMatrix() {
        
        // Hill Encryption --
        for (int i = 0; i < matrixSize; i++) {

            int number = 0; 
            for (int j = 0; j < matrixSize; j++) {
                number += Math.round(keyMatrix.get(i, j)) * originList.get(j).getNumberLetter(offset);
            }
            
            newList.get(i).setNumberLetter(number, offset);
        }
    }

}
