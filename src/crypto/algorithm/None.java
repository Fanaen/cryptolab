package crypto.algorithm;

/**
 *
 * @author Lifaen
 */


public class None extends Base {

    @Override
    public void encrypt() {
        message.setMessage(message.getMessage() + " -- Encrypted !");
    }

    @Override
    public void decrypt() {
        message.setMessage(message.getMessage() + " -- Decrypted !");
    }

    @Override
    public String getDataPanel() {
        return "SimpleKey";
    }
}
