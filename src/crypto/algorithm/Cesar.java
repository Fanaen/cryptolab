package crypto.algorithm;

import crypto.model.Char;

/**
 *
 * @author Lifaen
 */


public class Cesar extends BaseParseCharacter {
    
    public int rotate = 0;

    @Override
    public void decrypt() throws Exception {
        rotate = - (int) data;
        super.decrypt();
    }

    @Override
    public void encrypt() throws Exception {
        rotate = (int) data;
        super.encrypt();
    }

    
    @Override
    void encryptCharacter(Char c) {
        if(c.isLetter()) c.setNumberLetter(c.getNumberLetter() + rotate);
        message.getNewList().add(c);
    }

    @Override
    void decryptCharacter(Char c) {
        if(c.isLetter()) c.setNumberLetter(c.getNumberLetter() + rotate);
        message.getNewList().add(c);
    }

    @Override
    public String getDataPanel() {
        return "SimpleNumber";
    }
    
}
