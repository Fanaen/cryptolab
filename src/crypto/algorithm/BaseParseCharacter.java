package crypto.algorithm;

import crypto.model.Char;
import java.util.List;


/**
 *
 * @author Lifaen
 */


public abstract class BaseParseCharacter extends Base {

    @Override
    public void encrypt() throws Exception {
        for(Char c : message.getOriginList()) {
            encryptCharacter(c);
        }
        message.constructString();
    }
    
    abstract void encryptCharacter(Char c);

    @Override
    public void decrypt() throws Exception {
        for(Char c : message.getOriginList()) {
            decryptCharacter(c);
        }
        message.constructString();
    }
    
    abstract void decryptCharacter(Char c);
    
}
