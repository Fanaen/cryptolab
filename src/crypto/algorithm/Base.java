package crypto.algorithm;

import crypto.model.Message;

/**
 *
 * @author Lifaen
 */

abstract public class Base {
    
    // -- Attribute --
    protected Message message;
    protected Object data;
    
    // -- Methods --
    abstract public void encrypt() throws Exception;
    abstract public void decrypt() throws Exception;
    abstract public String getDataPanel();
    
    // -- Getters & Setters --
    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
