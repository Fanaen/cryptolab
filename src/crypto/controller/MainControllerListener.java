package crypto.controller;

/**
 *
 * @author Lifaen
 */

public interface MainControllerListener {
    
    // -- Methods --
    public void onError(Exception ex, String message);
    public void onConfigurationChanged(String configurationName);
    
    
    
}
