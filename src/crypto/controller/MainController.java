package crypto.controller;

import crypto.algorithm.Base;
import crypto.model.Message;
import java.awt.BorderLayout;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author Lifaen
 */


public class MainController {
    
    // -- Attributes --
    private List<String> algorithmList = new LinkedList<>();
    private String algorithmName;
    private Base algorithm;
    
    private List<MainControllerListener> listenerList = new LinkedList<>();
    
    // -- Constructor --
    public MainController() {
    }
    
    // -- Methods --
    public void prepare() {
        
        try {
            // Get the algorithm --
            String className = "crypto.algorithm." + algorithmName;
            Class<?> panelClass = Class.forName(className);
            algorithm = (Base) panelClass.newInstance();
            
            for (MainControllerListener listener : listenerList) {
                listener.onConfigurationChanged(algorithm.getDataPanel());
            }
            
        } catch (Exception ex) {
            catchException(ex);
        }
    }
    
    public String encrypt(String message, Object data) {
        try {
            // Get data --
            algorithm.setMessage(new Message(message));
            algorithm.setData(data);
            
            // Encrypt data --
            algorithm.encrypt();
            
            // Return data --
            return algorithm.getMessage().getMessage();
            
        } catch (Exception ex) {
            return catchException(ex);
        }
    }

    public String decrypt(String message, Object data) {
        try {
            // Get data --
            algorithm.setMessage(new Message(message));
            algorithm.setData(data);
            
            // Encrypt data --
            algorithm.decrypt();
            
            // Return data --
            return algorithm.getMessage().getMessage();
            
        } catch (Exception ex) {
            return catchException(ex);
        }
    }
    
    // -- Getters & Setters --
    public void addAlgorithm(String name) {
        if(!algorithmList.contains(name)) {
            algorithmList.add(name);
            algorithmName = name;
        }
    }
    
    public void requestAlgorithm(String name) {
        if(algorithmList.contains(name)) {
            algorithmName = name;
        }
    }
    
    public String getAlgorithmName() {
        return algorithmName;
    }
    
    public List<String> getAlgorithmList() {
        return algorithmList;
    }

    public void activateAlgorithm(String name) {
        
        // Add the algorithm if not already added --
        addAlgorithm(name);
        
        // Activate it --
        this.algorithmName = name;
        prepare();
    }

    public String getConfigurationName() {
        return (algorithm != null) ? algorithm.getDataPanel() : null;
    }
    
    public void addListener(MainControllerListener listener) {
        listenerList.add(listener);
    }

    private String catchException(Exception ex) {
        /*for (MainControllerListener listener : listenerList) {
            listener.onError(ex, message);
        }*/

        ex.printStackTrace();
        
        return "";
    }
}
