package crypto;

import crypto.controller.MainController;
import crypto.view.gui.MainView;



public class Crypto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
        final MainController controller = new MainController();
        
        controller.addAlgorithm("Cesar");
        controller.addAlgorithm("Hill");
        controller.requestAlgorithm("Cesar");
        
        MainView.launch(controller);
    }
}
